Ejecucion de API_REST

Rutas de la API_REST:

1.- mutation
2.- stats

NOTA: Para realizar la prueba se recomienda tener instalado POSTMAN, el cual realizada
      peticiones HTTP y pone a prueba las API REST.
NOTA: Tener instalado NodeJS ya que la API_REST  fue desarrollada en este lenguaje.

Configuracion de POSTMAN:

Ejecucion de ruta mutation:
* Configurar el metodo POST
  Ya que este ruta se ejecuta con unn HTTP POST es necesario seleccionarlo para asi lograr
  una correcta ejecucion de la API REST.
  
  
* Configurar los  Headers(Encabezados):
  KEY: Content-Type
  VALUE: application/json.
  
* Configuracion del Body:
  En esta seccion tendra que ir el objeto JSON con la secuencia de ADN  a analizar:
  Ejemplo:
         
         {
	      "SECADN": ["ATGGCGT","ATGCGAG","TCGCCGC","TTTGGGT","AAATAAA","GACGTTG"]
         }
         
Posteriormente una vez realizado lo anterior bastara con pulsar sobre el boton "SEND"
para enviar el archivo de informacion a la API_REST.

https://apirest-239101.appspot.com/mutation/
 
 
Ejemplo demostrativo:
Ejecucion de ruta stats:

* Configurar el metodo GET
  Seleccionar el metodo GET para hacer un uso correcto de la ruta /stats/

* Configurar los  Headers(Encabezados):
  KEY: Content-Type
  VALUE: application/json.
  
Una vez realizado lo anterior  pulsar sobre el boton "SEND" para ontener el JSON de 
respuesta con la informacion solicitada.

 Respuesta
 {
    "count_mutations": 1,
    "count_no_mutations": 3,
    "ratio": 0.3333333333333333
 } 

Ejemplo demostrativo:
https://apirest-239101.appspot.com/stats


URL DE LA API REST:

https://apirest-239101.appspot.com/













      